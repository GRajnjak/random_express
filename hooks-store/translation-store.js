import { initStore } from "./store";

const configureTranslationStore = () => {
  const actions = {
    SET_TRANSLATION_DATA: (curState, obj) => {
      let updatedTranslation = { ...curState.translations };
      updatedTranslation = obj;
      return { translations: updatedTranslation };
    },
    SET_THEMES: (curState, data) => {
      let updatedTranslation = { ...curState.translations };
      updatedTranslation.themes = data;
      return { translations: updatedTranslation };
    },
    SET_SCORE: (curState, data) => {
      let updatedTranslation = { ...curState.translations };
      updatedTranslation.score = data;
      return { translations: updatedTranslation };
    },
    SET_LANGUAGE: (curState, data) => {
      let updatedTranslation = { ...curState.translations };
      updatedTranslation.language = data;
      return { translations: updatedTranslation };
    },
    SET_WORD_ARRAY: (curState, data) => {
      let updatedTranslation = { ...curState.translations };
      updatedTranslation.wordArray = data;
      return { translations: updatedTranslation };
    },
    SET_INITIAL_WORD: (curState, data) => {
      let updatedTranslation = { ...curState.translations };
      updatedTranslation.initialWord = data;
      return { translations: updatedTranslation };
    },
    SET_INITIAL_RESULT: (curState, data) => {
      let updatedTranslation = { ...curState.translations };
      updatedTranslation.initialResult = data;
      return { translations: updatedTranslation };
    },
  };
  initStore(actions, {
    translations: {
      wordArray: [],
      language: "",
      themes: [],
      allThemesArray: [],
      score: 0,
      initialWord: "",
      initialResult: "",
    },
  });
};

export default configureTranslationStore;
