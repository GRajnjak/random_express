import React, { useState } from "react";
import classes from "./Theme.module.scss";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { faChevronUp } from "@fortawesome/free-solid-svg-icons";

import ThemeDrop from "./ThemeDrop/ThemeDrop";
import ThemeEditDrop from "./ThemeEditDrop/ThemeEditDrop";
import useComponentVisible from "../../hooks/useComponentVisible";

const Theme = (props) => {
  const { ref, isComponentVisible, setIsComponentVisible } =
    useComponentVisible(false);

  let headline;
  let current = null;

  const setVisibilityDropDown = () => {
    setIsComponentVisible(!isComponentVisible);
  };

  if (props.typeTheme === "translation" && props.themeCheck.length === 1) {
    headline = props.themeCheck[0];
  }
  if (props.typeTheme === "translation" && props.themeCheck.length > 1) {
    headline = "More themes (" + props.themeCheck.length + ")";
  }

  if (props.typeTheme === "translation" && props.themeCheck) {
    current = props.themeCheck;
  }
  if (props.typeTheme === "edits") {
    headline = props.chosenTheme;
  }

  let chevron = <FontAwesomeIcon icon={faChevronDown} />;

  if (isComponentVisible) {
    chevron = <FontAwesomeIcon icon={faChevronUp} />;
  }

  return (
    <div ref={ref} className={classes.Theme}>
      <div onClick={setVisibilityDropDown} className={classes.HeadButt}>
        <p>{headline}</p>
        <p>{chevron}</p>
      </div>
      {isComponentVisible && props.typeTheme === "translation" && (
        <React.Fragment>
          <ThemeDrop
            allThemesArray={props.allThemesArray}
            themeChoose={props.themeChoose}
            theme={props.themeCheck}
            currentTheme={current}
          />
        </React.Fragment>
      )}
      {isComponentVisible && props.typeTheme === "edits" && (
        <React.Fragment>
          <ThemeEditDrop
            allThemesArray={props.allThemesArray}
            themeChoose={props.themeClick}
            theme={props.themeCheck}
            chosenTheme={props.chosenTheme}
            dropDownMethod={setVisibilityDropDown}
            themeChooseMethod={props.themeChooseMethod}
          />
        </React.Fragment>
      )}
    </div>
  );
};

export default Theme;
